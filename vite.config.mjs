import { resolve } from 'path';
import mdx from '@mdx-js/rollup';
import { defineConfig } from 'vite';

const root = resolve(__dirname, 'src');
const outDir = resolve(__dirname, 'dist');

export default defineConfig({
  root,
  build: {
    outDir,
    emptyOutDir: true,
    rollupOptions: {
      input: {
        birdies: resolve(root, 'birds/index.html'),
        resume: resolve(root, 'resume/index.html'),
      },
      plugins: [mdx()],
    },
  },
});
