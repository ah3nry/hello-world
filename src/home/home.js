import gsap, { Bounce, Power2 } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';

gsap.registerPlugin(ScrollTrigger);
gsap.registerPlugin(ScrollToPlugin);

ScrollTrigger.clearScrollMemory();
window.history.scrollRestoration = 'manual';

const scene = document.getElementById('scene');
const poster = document.getElementById('poster');
const info = document.getElementById('info');
const posterFront = document.getElementById('poster-front');
const blocks = document.querySelectorAll('.block');
const grids = document.querySelectorAll('.grid');
const contentContainer = document.getElementById('content-container');

const tl1 = gsap.timeline({ delay: 0.5 });
const tl2 = gsap.timeline({ paused: true });
const tl3 = gsap.timeline({ paused: true, scrollTrigger: { trigger: info, start: '299' } });

gsap.set(poster, {
  transformStyle: 'preserve-3d',
  transformPerspective: 1000,
  // transform: 'translate3d(0, 8px, 0) rotateX(-10deg)',
});
gsap.set(info, {
  visibility: 'hidden',
  opacity: 0,
});
gsap.set(contentContainer, {
  // opacity: 0,
  // display: 'none',
  x: '150%',
});

tl1
  .to(poster, { duration: 0.1, x: 8, repeat: 5, ease: Power2.easeInOut, delay: 0.7 })
  .addLabel('shake')
  .to(
    grids,
    {
      duration: 0.4,
      gap: '2px',
    },
    'shake-=0.25',
  )
  .addLabel('gap')
  .to(
    blocks,
    {
      y: function (index) {
        switch (index) {
          case 1:
            return '40%';
          case 2:
            return '66.7%';
          case 3:
            return '26%';
          default:
            return 0;
        }
      },
      ease: Bounce.easeOut,
      stagger: 0.3,
    },
    'gap-=0.6',
  );

tl2
  .to(posterFront, { rotationY: 180 })
  .to(posterFront, { rotationX: 90 }, 0)
  // .to(posterFront, { backgroundColor: '#FFFFFF00' }, 0)
  .addLabel('rotation1')
  .to(
    posterFront,
    {
      scale: 1.2,
    },
    'rotation1',
  )
  // .to(posterFront, { boxShadow: 'none' }, 'rotation1')
  .to(posterFront, { rotationY: 180 }, 'rotation1')
  .to(posterFront, { rotationX: 180 }, 'rotation1')
  .to(posterFront, { rotationZ: -90 }, 'rotation1')
  .addLabel('rotation2')
  .to(
    posterFront,
    {
      scale: 1.5,
      x: '-180%',
    },
    'rotation2',
  )
  .addLabel('scale')
  .to(info, { visibility: 'visible', opacity: 1, height: '100%', width: '100vw' }, 'scale')
  .addLabel('info');
tl3
  .to(window, { duration: 0.1, scrollTo: { y: 0 } })
  .addLabel('scroll')
  .to(contentContainer, { duration: 0.3, display: 'block', opacity: 1, x: '0%' }, 'scroll');
// .to(posterFront, { opacity: 0 }, 'info')
// .to(contentContainer, { duration: 0.1, display: 'block', opacity: 1 }, 'info');

ScrollTrigger.create({
  animation: tl2,
  trigger: scene,
  start: 'top top',
  end: '+=300',
  pin: true,
  scrub: 1,
  onRefresh: (self) => self?.refresh(true),
  once: true,
  pinReparent: true,
  pinType: 'fixed',
  snap: {
    snapTo: 1 / 1, // progress increment
    // or "labels" or function or Array

    directional: true,
  },
});

// ScrollTrigger.create({
//   animation: tl3,
//   trigger: scene,
//   start: 'info',
//   // pin: true,
//   end: '+=100',
//   scrub: true,
// });

// function scene1() {
//   let tl = gsap.timeline();
//   tl.to(...).to(...); // build scene 1
//   return tl;
// }

// function scene2() {
//   let tl = gsap.timeline();
//   tl.to(...).to(...); // build scene 2
//   return tl;
// }

// let master = gsap.timeline()
//   .add(scene1())
//   .add(scene2(), "-=0.5")
