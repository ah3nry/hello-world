import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';


gsap.registerPlugin(ScrollTrigger);

const container = document.getElementById('vinyl-container');
const vinyl = document.getElementById('vinyl');
const tl = gsap.timeline({ paused: true });
tl.set(
  vinyl,
  {
    scale: 8,
    transformOrigin: 'center center',
    // duration: 1,
  },
  0,
);

// tl.to(
//     vinyl,
//   {
//     filter: 'saturate(10) hue-rotate(55deg)',
//     duration: 1,
//   },
//   0,
// );
tl.to(
  vinyl,
  {
    scale: 0.18,
    // delay: 1,
    duration: 3,
  },
  0,
);
const trigger = ScrollTrigger.create({
  animation: tl,
  trigger: container,
  start: 'top top',
  end: '+=150',
  markers: false,
  // pin: "#pinned-hero",
  pin: true,
  scrub: true,
});
