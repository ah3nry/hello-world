import{j as a}from"./color-scheme-98ec2cc8.js";import{M as i}from"./chunk-HLWAVYOI-9b648bd5.js";import{u as t}from"./index-f4ac87fd.js";import"./iframe-8d91e94b.js";import"../sb-preview/runtime.js";import"./_commonjsHelpers-725317a4.js";import"./index-d37d4223.js";import"./index-d38538b0.js";import"./index-356e4a49.js";const o=""+new URL("hair-e60facdd.png",import.meta.url).href;function n(r){return a.jsxs(a.Fragment,{children:[a.jsx(i,{title:"Tokens/Elevation",parameters:{viewMode:"docs",previewTabs:{canvas:{hidden:!0}}}}),`
`,a.jsx("style",{children:`
  div#story--atoms-colors--colors {
    display: none;
  }
  .color_wrapper {
    display: flex;
    flex-direction: column;
    min-width: 873px;
  }
  .swatch-wrapper {
    display: flex;
  }
  .swatch-box {
    min-width: 800px;
    overflow: auto;
  }
  .swatch {
    display: flex;
    height: 120px;
    margin-bottom: var(--ah3nry-spacing-4);
  }
   .color__item {
    height: 100%;
    width: 50px;
    display: flex;
    justify-content: center;
  }
  .swatch-label{
    width: 120px;
    margin-right: var(--ah3nry-spacing-4);
  }
  .color__code{
    font-size: 13px;
    color: #666;
  }
  .color-wrapper {
    display:none
  }
  .header {
    padding: var(--ah3nry-spacing-13);
    background-image: url(${o});
    background-size: cover;
    background-position: bottom;
    background-color: #f9f9f9;

  }
  .title {
    padding: var(--ah3nry-spacing-13) 0;
  }
  section {
    padding: var(--ah3nry-spacing-8) calc(var(--ah3nry-spacing-10) * 2);
  }
  .theme-container {
    background: #f8f9ff;
    padding: var(--ah3nry-spacing-4) var(--ah3nry-spacing-8);
    width: 75%;
    border: 1px solid  var( --ah3nry-palette-neutral90);
    border-radius: var(--ah3nry-shape-corner-medium-default-size);
  }
  
  `}),`
`,a.jsx("header",{className:"header shape-medium",children:a.jsx("div",{className:"title",children:a.jsx("h1",{className:"display-large",children:"Elevation"})})})]})}function x(r={}){const{wrapper:e}=Object.assign({},t(),r.components);return e?a.jsx(e,Object.assign({},r,{children:a.jsx(n,r)})):n()}export{x as default};
//# sourceMappingURL=elevation-6514ca5f.js.map
