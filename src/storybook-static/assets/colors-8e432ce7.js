import{j as e}from"./color-scheme-98ec2cc8.js";import{M as o}from"./chunk-HLWAVYOI-9b648bd5.js";import{u as n}from"./index-f4ac87fd.js";import"./iframe-8d91e94b.js";import"../sb-preview/runtime.js";import"./_commonjsHelpers-725317a4.js";import"./index-d37d4223.js";import"./index-d38538b0.js";import"./index-356e4a49.js";const i=""+new URL("franc-4d581f55.webp",import.meta.url).href,s=""+new URL("light-theme-84983f57.png",import.meta.url).href,l=""+new URL("dark-theme-d3bcf893.png",import.meta.url).href,c=""+new URL("tones-bbf99e6a.png",import.meta.url).href,b=r=>r.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase(),h=r=>r.replace(/([a-z])([A-Z])/g,"$1 $2").split(" "),d=r=>r.charAt(0).toUpperCase()+r.slice(1),v=r=>h(r).map(d).join("  ");function a(r){const t=Object.assign({p:"p",a:"a",h3:"h3"},n(),r.components);return e.jsxs(e.Fragment,{children:[e.jsx(o,{title:"Tokens/Colors",parameters:{viewMode:"docs",previewTabs:{canvas:{hidden:!0}}}}),`
`,e.jsx("style",{children:`
  div#story--atoms-colors--colors {
    display: none;
  }
  .color_wrapper {
    display: flex;
    flex-direction: column;
    min-width: 873px;
  }
  .swatch-wrapper {
    display: flex;
  }
  .swatch-box {
    min-width: 800px;
    overflow: auto;
  }
  .swatch {
    display: flex;
    height: 120px;
    margin-bottom: var(--ah3nry-spacing-4);
  }
   .color__item {
    height: 100%;
    width: 50px;
    display: flex;
    justify-content: center;
  }
  .swatch-label{
    width: 120px;
    margin-right: var(--ah3nry-spacing-4);
  }
  .color__code{
    font-size: 13px;
    color: #666;
  }
  .color-wrapper {
    display:none
  }
  .header {
    padding: var(--ah3nry-spacing-13);
    background-image: url(${i});
    background-size: cover;
    background-color: #f9f9f9;

  }
  .title {
    padding: var(--ah3nry-spacing-13) 0;
  }
  section {
    padding: var(--ah3nry-spacing-8) calc(var(--ah3nry-spacing-10) * 2);
    display: flex;
    flex-direction: column;
    max-width: 1200px;
    margin: 0 auto;
  }
  .theme-container {
    background: #f8f9ff;
    padding: var(--ah3nry-spacing-4) var(--ah3nry-spacing-8);
    width: 75%;
    border: 1px solid  var( --ah3nry-palette-neutral90);
    border-radius: var(--ah3nry-shape-corner-medium-default-size);
    align-self: center;
  }
  
  `}),`
`,e.jsx("header",{className:"header shape-medium",children:e.jsx("div",{className:"title",children:e.jsx("h1",{className:"display-large",children:"Colour"})})}),`
`,e.jsxs("section",{children:[e.jsxs("p",{children:[e.jsxs(t.p,{children:["The color scheme was built using the Material Theme Builder in Figma. Check out the Materail3 ",e.jsx(t.a,{href:"https://m3.material.io/styles/color/overview",target:"_blank",rel:"nofollow noopener noreferrer",children:"docs"})," for more imformation about how colour is used in the Material Design System."]}),e.jsxs(t.p,{children:["All colours are defined as CSS custom properties on the ",e.jsx("code",{children:":root"})," element. See below for tonal palette and light and dark colour schemes."]})]}),e.jsx("h2",{children:"Tonal Palettes"}),e.jsx(t.p,{children:`A tonal palette comprises a total of thirteen tones, which encompasses both white and black.
The 100 tone consistently represents 100% white, marking the lightest tone within the spectrum.
On the other hand, the 0 tone signifies 100% black, representing the darkest tone available.`}),e.jsxs(t.p,{children:[`Every tone value within the range of 0 to 100 quantifies the degree of lightness inherent in the color.
Each color's tonal quality is conveyed using the numerical designation corresponding to that specific role.
For instance, "primary40" designates the primary key color with a tonal value of 40 and
is available as the custom property `,e.jsx("code",{children:"--ah3nry-palette-primary40"})," in the CSS."]}),e.jsx("div",{className:"theme-container",children:e.jsx("img",{src:c})}),e.jsx("h2",{children:"Modes"}),e.jsx(t.h3,{id:"light-mode",children:"Light Mode"}),e.jsx("div",{className:"theme-container",children:e.jsx("img",{src:s})}),e.jsx(t.h3,{id:"dark-mode",children:"Dark Mode"}),e.jsx("div",{className:"theme-container",style:{backgroundColor:"#373434"},children:e.jsx("img",{src:l})})]})]})}function k(r={}){const{wrapper:t}=Object.assign({},n(),r.components);return t?e.jsx(t,Object.assign({},r,{children:e.jsx(a,r)})):a(r)}export{d as capitalized,k as default,v as formatName,h as splitCamelCase,b as toKebabCase};
//# sourceMappingURL=colors-8e432ce7.js.map
