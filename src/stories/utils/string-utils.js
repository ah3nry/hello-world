const addStringMethods = () => {
    String.prototype.toKebabCase = function(str) {
        return str.replace(/([a-z])([A-Z])/g, "$1-$2").toLowerCase();
    }

    String.prototype.capitalized = function(word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    }
}

export default addStringMethods;