import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
// import "./apple.scss";

gsap.registerPlugin(ScrollTrigger);

const pinnedImages = document.querySelectorAll('.pinned-image');

pinnedImages.forEach((pinnedImage) => {
  const navbar = document.querySelector('.navbar');
  const container = pinnedImage.querySelector('.pinned-image__container');
  // const image = container.querySelector('img');
  const overlay = container.querySelector('.pinned-image__container-overlay');
  const quote = pinnedImage.querySelector('.pinned-image__quote');
  const tl = gsap.timeline({ paused: true });
  tl.to(
    container,
    {
      scale: 0.6,
    },
    0,
  );
  tl.from(
    quote,
    {
      autoAlpha: 0,
    },
    0,
  );
  tl.from(
    overlay,
    {
      autoAlpha: 0,
    },
    0,
  );
  const trigger = ScrollTrigger.create({
    animation: tl,
    trigger: pinnedImage,
    start: 'top top',
    end: '+=300',
    markers: false,
    // pin: "#pinned-hero",
    pin: true,
    scrub: true,
  });
});
